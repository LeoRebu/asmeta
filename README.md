# asmeta
asmeta main repository with code and examples

## Requirements

To use the asmeta framework in Eclipse IDE you need to install:

* Zest SDK 1.7
* XText SDK
* Eclipse Plug-in Development Environment
* JavaCC Eclipse Plug-in