/*
 * generated by Xtext 2.21.0
 */
package org.asmeta.xt.ui;

import com.google.inject.Injector;
import org.asmeta.xt.ui.internal.XtActivator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class AsmetaLExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return Platform.getBundle(XtActivator.PLUGIN_ID);
	}
	
	@Override
	protected Injector getInjector() {
		XtActivator activator = XtActivator.getInstance();
		return activator != null ? activator.getInjector(XtActivator.ORG_ASMETA_XT_ASMETAL) : null;
	}

}
