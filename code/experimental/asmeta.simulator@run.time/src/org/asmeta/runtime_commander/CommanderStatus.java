package org.asmeta.runtime_commander;

public enum CommanderStatus {
	FAILURE,
	SIM_ID,
	LOADED_IDS,
	INSTANCES,
	STOP,
	RUNOUTPUT,
	VIEWINV,
	BOOLRES;
}
